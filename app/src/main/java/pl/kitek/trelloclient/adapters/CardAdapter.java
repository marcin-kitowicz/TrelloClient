package pl.kitek.trelloclient.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.kitek.trelloclient.R;
import pl.kitek.trelloclient.models.TodoCard;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    private List<TodoCard> cards;
    private View.OnClickListener onClickListener;

    public CardAdapter(List<TodoCard> cards) {
        this.cards = cards;
    }

    public void setMoreBtnClickListener(View.OnClickListener listener) {
        onClickListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.cardTitle) TextView title;
        @Bind(R.id.cardDescription) TextView description;
        @Bind(R.id.moreBtn) View moreBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        TodoCard card = getItem(position);

        holder.title.setText(card.getName());

        if (TextUtils.isEmpty(card.getDesc())) {
            holder.description.setVisibility(View.GONE);
        } else {
            holder.description.setVisibility(View.VISIBLE);
            holder.description.setText(card.getDesc());
        }
        if (null != onClickListener) {
            holder.moreBtn.setTag(position);
            holder.moreBtn.setOnClickListener(onClickListener);
        }
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public TodoCard getItem(int position) {
        return cards.get(position);
    }

}
