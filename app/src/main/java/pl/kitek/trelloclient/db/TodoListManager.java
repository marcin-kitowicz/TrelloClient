package pl.kitek.trelloclient.db;

import android.support.annotation.NonNull;

import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

import pl.kitek.trelloclient.models.TodoList;
import timber.log.Timber;

public class TodoListManager {

    private final DatabaseHelper db;

    public TodoListManager(DatabaseHelper databaseHelper) {
        this.db = databaseHelper;
    }

    public void removeAll() throws SQLException {
        TableUtils.clearTable(db.getConnectionSource(), TodoList.class);
    }

    public void add(@NonNull List<TodoList> todoLists) throws SQLException {
        int size = todoLists.size();
        for (int i = 0; i < size; i++) {
            // TODO: 10.08.15 Start transaction or smth?
            db.getTodoListDao().createOrUpdate(todoLists.get(i));

            Timber.d("add " + todoLists.get(i).toString());
        }
    }

    public List<TodoList> getAll() throws SQLException {
        return db.getTodoListDao().queryForAll();
    }

}
