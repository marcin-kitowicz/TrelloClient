package pl.kitek.trelloclient.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import pl.kitek.trelloclient.models.TodoCard;
import pl.kitek.trelloclient.models.TodoList;
import timber.log.Timber;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "trello.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<TodoList, String> todoListDao;
    private Dao<TodoCard, String> todoCardDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, TodoList.class);
            TableUtils.createTable(connectionSource, TodoCard.class);
        } catch (SQLException e) {
            Timber.e(e, "onCreate table ERROR occurred");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, TodoList.class, true);
            TableUtils.dropTable(connectionSource, TodoCard.class, true);

            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Timber.e(e, "onUpgrade table ERROR occurred");
        }
    }

    public Dao<TodoList, String> getTodoListDao() throws SQLException {
        if (todoListDao == null) {
            todoListDao = getDao(TodoList.class);
        }
        return todoListDao;
    }

    public Dao<TodoCard, String> getTodoCardDao() throws SQLException {
        if (todoCardDao == null) {
            todoCardDao = getDao(TodoCard.class);
        }
        return todoCardDao;
    }

    @Override
    public void close() {
        super.close();
        todoListDao = null;
        todoCardDao = null;
    }
}
