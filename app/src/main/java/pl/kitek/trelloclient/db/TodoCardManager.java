package pl.kitek.trelloclient.db;

import android.support.annotation.NonNull;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

import pl.kitek.trelloclient.models.TodoCard;

public class TodoCardManager {

    private final DatabaseHelper db;

    public TodoCardManager(DatabaseHelper db) {
        this.db = db;
    }

    public void removeAll() throws SQLException {
        // TODO: 11.08.15 W tym miejscu powinny zostac usuniete tylko karty zdalne (pomimin lokalne)
        TableUtils.clearTable(db.getConnectionSource(), TodoCard.class);
    }

    public void add(@NonNull List<TodoCard> todoCards) throws SQLException {
        int size = todoCards.size();
        for (int i = 0; i < size; i++) {
            db.getTodoCardDao().createOrUpdate(todoCards.get(i));
        }
    }

    public List<TodoCard> getAll(@NonNull String listId) throws SQLException {
        QueryBuilder<TodoCard, String> qb = db.getTodoCardDao().queryBuilder();
        qb.orderBy(TodoCard.POS, true);

        Where<TodoCard, String> where = qb.where();
        where.eq(TodoCard.ID_LIST, listId);

        return db.getTodoCardDao().query(qb.prepare());
    }


    public void update(@NonNull List<TodoCard> todoCards) throws SQLException {
        QueryBuilder<TodoCard, String> qb = db.getTodoCardDao().queryBuilder();
        // Dupa trupa
    }





}
