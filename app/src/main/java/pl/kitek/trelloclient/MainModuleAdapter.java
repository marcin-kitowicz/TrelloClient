package pl.kitek.trelloclient;

import android.content.Context;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.kitek.trelloclient.db.DatabaseHelper;
import pl.kitek.trelloclient.db.TodoCardManager;
import pl.kitek.trelloclient.db.TodoListManager;
import pl.kitek.trelloclient.fragments.CardListFragment;
import pl.kitek.trelloclient.sync.TrelloApi;
import pl.kitek.trelloclient.sync.TrelloService;
import pl.kitek.trelloclient.ui.MainActivity;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

@Module(injects = {
        MainActivity.class,
        CardListFragment.class,
        TrelloService.class
}, library = true)
public class MainModuleAdapter {

    private final Context context;

    public MainModuleAdapter(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Singleton
    @Provides
    public Bus provideBus() {
        return new Bus();
    }

    @Provides
    @Singleton
    public DatabaseHelper provideDatabaseHelper(Context context) {
        return new DatabaseHelper(context);
    }

    @Provides
    @Singleton
    public TodoListManager provideTodoListManager(DatabaseHelper db) {
        return new TodoListManager(db);
    }

    @Provides
    @Singleton
    public TodoCardManager provideTodoCardManager(DatabaseHelper db) {
        return new TodoCardManager(db);
    }

    @Singleton
    @Provides
    public TrelloApi provideTrelloApi() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(TrelloApi.API_URL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addQueryParam("key", TrelloApi.API_KEY);
                    }
                })
                .build();

        return restAdapter.create(TrelloApi.class);
    }

}
