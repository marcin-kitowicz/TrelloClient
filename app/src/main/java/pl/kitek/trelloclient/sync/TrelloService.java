package pl.kitek.trelloclient.sync;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.squareup.otto.Bus;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import pl.kitek.trelloclient.App;
import pl.kitek.trelloclient.db.TodoCardManager;
import pl.kitek.trelloclient.db.TodoListManager;
import pl.kitek.trelloclient.events.SyncFailEvent;
import pl.kitek.trelloclient.events.SyncSuccessEvent;
import pl.kitek.trelloclient.events.TodoEvent;
import pl.kitek.trelloclient.models.TodoCard;
import pl.kitek.trelloclient.models.TodoList;
import timber.log.Timber;

public class TrelloService extends Service {

    @Inject Bus bus;
    @Inject TrelloApi trelloApi;
    @Inject TodoListManager todoListManager;
    @Inject TodoCardManager todoCardManager;

    private SyncTask syncTask;

    @Override
    public void onCreate() {
        super.onCreate();
        App.inject(this);

        Timber.d("onCreate");

        syncTask = new SyncTask();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (syncTask.getStatus() != AsyncTask.Status.RUNNING) {
            syncTask.execute();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Timber.d("onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private class SyncTask extends AsyncTask<Void, Void, TodoEvent> {

        @Override
        protected TodoEvent doInBackground(Void... params) {
            Timber.d("doInBackground ");

            TodoEvent event = new SyncSuccessEvent();

            try {
                updateTodoLists();
                updateTodoCards();

                // TODO: 11.08.15 Sync others

            } catch (SQLException e) {
                event = new SyncFailEvent();

                Timber.e(e, "doInBackground Error");
            }

            return event;
        }

        @Override
        protected void onPostExecute(TodoEvent event) {
            super.onPostExecute(event);

            if (event != null) {
                bus.post(event);
            }
            stopSelf();
        }
    }

    private void updateTodoLists() throws SQLException {
        todoListManager.removeAll();
        List<TodoList> lists = trelloApi.getLists(TrelloApi.BOARD_ID);

        if (lists != null) {
            todoListManager.add(lists);

            Timber.d("getTodoLists added");
        }
    }

    private void updateTodoCards() throws SQLException {
        List<TodoList> lists = todoListManager.getAll();
        int size = lists.size();
        if (size > 0) {

            todoCardManager.removeAll();

            for (int i = 0; i < size; i++) {
                List<TodoCard> cards = trelloApi.getCards(lists.get(i).getId());

                Timber.d("updateTodoCards #id: " + lists.get(i).getId() + " size: " + cards.size());

                todoCardManager.add(cards);
            }
        }
    }
    
}
