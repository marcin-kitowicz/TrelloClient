package pl.kitek.trelloclient.sync;

import java.util.List;

import pl.kitek.trelloclient.models.TodoCard;
import pl.kitek.trelloclient.models.TodoList;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface TrelloApi {

    String BOARD_ID = "N1GSWXIq";
    String API_URL = "https://api.trello.com/1/";
    String API_KEY = "0538f27f1e115986d747a7192a443ce9";

    @GET("/board/{board}/lists")
    void getLists(@Path("board") String boardId, Callback<List<TodoList>> cb);

    @GET("/board/{board}/lists")
    List<TodoList> getLists(@Path("board") String boardId);

    @GET("/lists/{list}/cards")
    List<TodoCard> getCards(@Path("list") String listId);
}
