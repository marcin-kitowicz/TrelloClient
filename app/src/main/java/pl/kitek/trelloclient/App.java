package pl.kitek.trelloclient;

import android.app.Application;

import dagger.ObjectGraph;
import timber.log.Timber;

public class App extends Application {

    private static ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }


        objectGraph = ObjectGraph.create(new MainModuleAdapter(getApplicationContext()));
    }


    public static void inject(Object object) {
        objectGraph.inject(object);
    }
}
