package pl.kitek.trelloclient.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.sql.SQLException;

import javax.inject.Inject;

import pl.kitek.trelloclient.App;
import pl.kitek.trelloclient.R;
import pl.kitek.trelloclient.adapters.CardAdapter;
import pl.kitek.trelloclient.db.TodoCardManager;
import pl.kitek.trelloclient.models.TodoCard;
import timber.log.Timber;

public class CardListFragment extends Fragment implements View.OnClickListener {

    private static final String ID = "pl.kitek.trelloclient.fragments.CardListFragment.ID";

    private String listId;
    private CardAdapter cardAdapter;

    @Inject TodoCardManager todoCardManager;
    private AlertDialog optionsDialog;

    public static CardListFragment newInstance(String listId) {
        Bundle bundle = new Bundle();
        bundle.putString(ID, listId);

        CardListFragment cardListFragment = new CardListFragment();
        cardListFragment.setArguments(bundle);

        return cardListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.inject(this);

        Bundle arguments = getArguments();
        if (arguments != null) {
            listId = arguments.getString(ID);
        }

        try {
            cardAdapter = new CardAdapter(todoCardManager.getAll(listId));
            cardAdapter.setMoreBtnClickListener(this);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_card_list, container, false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(cardAdapter);

        return recyclerView;
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        TodoCard item = cardAdapter.getItem(position);
        Timber.d("onCardClick " + position + " " + item.getName());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(item.getName())
                .setItems(R.array.card_options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                // edit
                                break;
                            case 1:
                                // move
                                break;
                            case 2:
                                // delete
                                break;
                        }
                    }
                });
        optionsDialog = builder.create();
        optionsDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (optionsDialog != null && optionsDialog.isShowing()) {
            optionsDialog.dismiss();
        }
    }
}
