package pl.kitek.trelloclient.models;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "todo_card")
public class TodoCard {

    public static final String ID = "id";
    public static final String ID_LIST = "id_list";
    public static final String NAME = "name";
    public static final String DESC = "desc";
    public static final String POS = "pos";

    @Expose @DatabaseField(id = true, columnName = ID)
    private String id;

    @Expose @DatabaseField(canBeNull = false, columnName = ID_LIST)
    private String idList;

    @Expose @DatabaseField(canBeNull = false, columnName = NAME)
    private String name;

    @Expose @DatabaseField(canBeNull = true, columnName = DESC)
    private String desc;

    @Expose @DatabaseField(canBeNull = false, columnName = POS)
    private int pos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdList() {
        return idList;
    }

    public void setIdList(String idList) {
        this.idList = idList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
